from graphene_django.debug import DjangoDebug

import users.schema
import notes.schema

import graphene


class Query(users.schema.Query, notes.schema.Query):
    debug = graphene.Field(DjangoDebug, name='_debug')


class Mutation(users.schema.Mutation, notes.schema.Mutation):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
