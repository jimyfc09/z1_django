from django.utils.translation import ugettext as _
from django.db import models

from model_utils import Choices

from users.models import CustomUser as User


class Note(models.Model):
    VAR_STATUS = Choices(
        ('public', 'PUBL', _('Public')),
        ('protected', 'PROT', _('Protected')),
        ('private', 'PRIV', _('Private')),
    )
    user = models.ForeignKey(User, null=False, blank=False, on_delete=models.CASCADE)
    note = models.TextField(blank=True, null=True)
    status = models.CharField(max_length=15, choices=VAR_STATUS, default=VAR_STATUS.PUBL)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.note[:25]

    class Meta:
        verbose_name_plural = _("Notes")
