from django.utils.translation import ugettext as _

from graphene_django import DjangoObjectType

from .filters import NoteFilter
from .models import Note

import graphene


class NoteType(DjangoObjectType):

    class Meta:
        model = Note
        fields = ('id', 'user', 'note', 'status', 'created')


class NoteNode(DjangoObjectType):

    class Meta:
        model = Note
        filterset_class = NoteFilter
        interfaces = (graphene.relay.Node, )
        fields = ('id', 'user', 'note', 'status', 'created')
