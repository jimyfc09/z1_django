from django.utils.translation import ugettext as _
from django.forms import forms, ModelForm

from .models import Note


class NoteForm(ModelForm):
    
    def __init__(self, *args, **kwargs):
        self._user = kwargs.pop('user', None)
        super(NoteForm, self).__init__(*args, **kwargs)

    def clean(self):
        if self._user is None or not self._user.is_authenticated:
            raise forms.ValidationError(_('Unauthenticated user'))
        return self.cleaned_data

    def save(self, *args, **kwargs):
        if self._user is None:
            return None
        self.instance.user = self._user
        return super(NoteForm, self).save(*args, **kwargs)
    
    class Meta:
        model = Note
        fields = ['note', 'status']
        required = ['note', 'status']