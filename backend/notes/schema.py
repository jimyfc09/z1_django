from django.db.models import Q
from django.utils.translation import ugettext as _

from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.forms.mutation import DjangoModelFormMutation
from graphene_django.types import ErrorType

from users.models import Follower
from .types import NoteNode, NoteType
from .forms import NoteForm
from .models import Note

import graphene


class CreateNoteMutation(DjangoModelFormMutation):
    success = graphene.Boolean()
    note = graphene.Field(NoteType)

    @classmethod
    def mutate_and_get_payload(self, root, info, **input):
        form = self.get_form(root, info, **input)
        if form.is_valid():
            return self.perform_mutate(form, info)
        else:
            errors = ErrorType.from_errors(form.errors)
            return self(errors=errors, success=False)

    @classmethod
    def perform_mutate(self, form, info):
        self.success = True
        self.error = None
        obj = form.save()
        kwargs = {self._meta.return_field_name: obj}
        return self(errors=[], success=True, **kwargs)

    @classmethod
    def get_form_kwargs(self, root, info, **input):
        kwargs = {
            'data': input,
            'user': info.context.user
        }
        return kwargs

    class Meta:
        form_class = NoteForm


class DeleteNoteMutation(graphene.Mutation):
    note_id = graphene.Int()
    success = graphene.Boolean()
    errors = graphene.List(ErrorType)

    class Arguments:
        id = graphene.Int()

    @classmethod
    def mutate(self, root, info, id):
        user = info.context.user
        if not user or not user.is_authenticated:
            return self(errors=[{'messages': [_('Unauthenticated user')]}], success=False)
        try:
            note = Note.objects.get(id=id, user=user)
            note.delete()
            return DeleteNoteMutation(note_id=id, errors=[], success=True)
        except Note.DoesNotExist:
            return self(errors=[{'messages': [_('Note does not exist')]}])


class ChangeStatusNoteMutation(graphene.Mutation):
    note = graphene.Field(NoteType)
    errors = graphene.List(ErrorType)
    success = graphene.Boolean()

    class Arguments:
        id = graphene.ID()
        status = graphene.String(required=True)

    @classmethod
    def mutate(self, root, info, id, status):
        errors = []
        user = info.context.user
        if not user or not user.is_authenticated:
            errors.append(_('Unauthenticated user'))
        else:
            try:
                Note.VAR_STATUS[status]
                try:
                    note = Note.objects.get(pk=id, user=user)
                    note.status = status
                    note.save()
                    return ChangeStatusNoteMutation(note=note, success=True, errors=[])
                except Note.DoesNotExist:
                    errors.append(_('Note does not exist'))
                    return ChangeStatusNoteMutation(errors=errors, success=False)
            except KeyError:
                errors.append(_('Invalid status'))
        return self(errors=[{'messages': errors}], success=False)


class Query(graphene.ObjectType):
    all_notes = DjangoFilterConnectionField(NoteNode)

    def resolve_all_notes(self, info, **kwargs):
        user = info.context.user
        if not user or not user.is_authenticated:
            return Note.objects.filter(status=Note.VAR_STATUS.PUBL)
        else:
            following = Follower.objects.filter(follower=user).values_list('me__pk', flat=True)
            return Note.objects.filter(
                Q(pk__in=Note.objects.filter(user=user).values_list('pk', flat=True)) |
                Q(
                    pk__in=Note.objects.filter(
                        user__in=following, status__in=[Note.VAR_STATUS.PUBL, Note.VAR_STATUS.PRIV]
                    )
                )
            ).order_by('-created')


class Mutation(graphene.ObjectType):
    create_note = CreateNoteMutation.Field()
    delete_note = DeleteNoteMutation.Field()
    change_status_note = ChangeStatusNoteMutation.Field()
