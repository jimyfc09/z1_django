#!/bin/bash

set -e 
sleep 5
echo "starting makemigrations"
#./manage.py makemigrations audiovisual --settings=${DJANGO_SETTINGS_MODULE}
./manage.py collectstatic --noinput --settings=${ DJANGO_SETTINGS_MODULE}
gunicorn --workers=5 --threads=2 --log-level=error --access-logfile /logs/access.log  --error-logfile /logs/error.log, --log-file /logs/gunicorn.log -b 0.0.0.0:9000 z1.wsgi:application