from graphene_django import DjangoObjectType

from .filters import FollowerFilter
from .models import CustomUser as User, Follower

import graphene


class UserType(DjangoObjectType):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name')


class NewUserType(DjangoObjectType):
    class Meta:
        model = User
        fields = ('id', 'email', 'username', 'first_name', 'password')


class FollowerNode(DjangoObjectType):
    class Meta:
        model = Follower
        filterset_class = FollowerFilter
        interfaces = (graphene.relay.Node, )
        fields = ['id', 'me', 'follower', 'status']


class FollowerType(DjangoObjectType):
    class Meta:
        model = Follower
        fields = '__all__'
