from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext as _

from model_utils import Choices


class CustomUser(AbstractUser):

    email = models.EmailField(blank=False, max_length=254, verbose_name="email address")

    USERNAME_FIELD = "username"
    EMAIL_FIELD = "email"


class Follower(models.Model):
    VAR_STATUS = Choices(
        (1, 'RECE', _('Received')),
        (2, 'APPR', _('Approved')),
        (3, 'DENI', _('Denied'))
    )
    me = models.ForeignKey(
        CustomUser, related_name='followers_me', null=True, blank=True, default=None, on_delete=models.CASCADE
    )
    follower = models.ForeignKey(
        CustomUser, related_name='my_followers', null=True, blank=True, default=None, on_delete=models.CASCADE
    )
    status = models.IntegerField(choices=VAR_STATUS, default=VAR_STATUS.RECE)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = _('Followers')
