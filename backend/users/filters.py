from django.db.models import Q
from django_filters import rest_framework as filters

from .models import Follower


class FollowerFilter(filters.FilterSet):
    search_user = filters.CharFilter(method='filter_search_user')

    def filter_search_user(self, queryset, name, value):
        return queryset.filter(
            Q(me__username__icontains=value) |
            Q(me__first_name__icontains=value) |
            Q(follower__username__icontains=value) |
            Q(follower__first_name__icontains=value)
        )

    class Meta:
        model = Follower
        fields = ['status', 'me', 'follower', 'search_user']