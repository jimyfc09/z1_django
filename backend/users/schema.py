from django.utils.translation import ugettext as _
from django.db.models import Q

from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.forms.mutation import DjangoModelFormMutation
from graphene_django.types import ErrorType
from graphql_auth import mutations

from .types import NewUserType, UserType, FollowerNode, FollowerType
from .models import CustomUser as User, Follower
from .forms import UserForm, FollowerForm

import graphene


class CreateUser(DjangoModelFormMutation):
    user = graphene.Field(NewUserType)

    class Meta:
        form_class = UserForm


class FollowMutate(DjangoModelFormMutation):
    success = graphene.Boolean()
    follower = graphene.Field(FollowerType)
    follower = graphene.Field(FollowerType)

    @classmethod
    def mutate_and_get_payload(self, root, info, **input):
        form = self.get_form(root, info, **input)
        if form.is_valid():
            return self.perform_mutate(form, info)
        else:
            errors = ErrorType.from_errors(form.errors)
            return self(errors=errors, success=False)

    @classmethod
    def perform_mutate(self, form, info):
        self.success = True
        self.error = None
        obj = form.save()
        kwargs = {self._meta.return_field_name: obj}
        return self(errors=[], success=True, **kwargs)

    @classmethod
    def get_form_kwargs(self, root, info, **input):
        kwargs = {
            'data': input,
            'follower': info.context.user
        }
        return kwargs

    class Meta:
        form_class = FollowerForm


class StopFollowingMutate(graphene.Mutation):
    followed_id = graphene.Int()
    success = graphene.Boolean()
    errors = graphene.List(ErrorType)

    class Arguments:
        followed_id = graphene.Int()

    @classmethod
    def mutate(self, root, info, followed_id):
        user = info.context.user
        if not user or not user.is_authenticated:
            return self(errors=[{'messages': [_('Unauthenticated user')]}], success=False)
        try:
            followed = Follower.objects.get(me__id=followed_id, follower=user)
            followed.delete()
            return StopFollowingMutate(followed_id=followed_id, errors=[], success=True)
        except Follower.DoesNotExist:
            return self(errors=[{'messages': [_('Register does not exist')]}])


class RemoveFollowerMutate(graphene.Mutation):
    follower_id = graphene.Int()
    success = graphene.Boolean()
    errors = graphene.List(ErrorType)

    class Arguments:
        follower_id = graphene.Int()

    @classmethod
    def mutate(self, root, info, follower_id):
        user = info.context.user
        if not user or not user.is_authenticated:
            return self(errors=[{'messages': [_('Unauthenticated user')]}], success=False)
        try:
            followed = Follower.objects.get(follower__id=follower_id, me=user)
            followed.delete()
            return RemoveFollowerMutate(follower_id=follower_id, errors=[], success=True)
        except Follower.DoesNotExist:
            return self(errors=[{'messages': [_('Register does not exist')]}])


class AuthMutation(graphene.ObjectType):
    register = mutations.Register.Field()
    # verify_account = mutations.VerifyAccount.Field()
    send_password_reset_email = mutations.SendPasswordResetEmail.Field()
    token_auth = mutations.ObtainJSONWebToken.Field()
    password_change = mutations.PasswordChange.Field()
    verify_token = mutations.VerifyToken.Field()


class Query(graphene.ObjectType):
    users = graphene.List(UserType)
    followers = DjangoFilterConnectionField(FollowerNode)
    followed = DjangoFilterConnectionField(FollowerNode)

    def resolve_users(self, info, **kwargs):
        return User.objects.all()

    def resolve_followers(self, info, **kwargs):
        user = info.context.user
        return Follower.objects.filter(me=user)

    def resolve_followed(self, info, **kwargs):
        user = info.context.user
        return Follower.objects.filter(follower=user)


class Mutation(AuthMutation, graphene.ObjectType):
    create_user = CreateUser.Field()
    follow = FollowMutate.Field()
    stop_following = StopFollowingMutate.Field()
    remove_follower = RemoveFollowerMutate.Field()
