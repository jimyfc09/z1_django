from django.utils.translation import ugettext as _
from django.forms import forms, ModelForm

from .models import CustomUser as User, Follower


class UserForm(ModelForm):

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(username=email).exists():
            raise forms.ValidationError('Usuario ya existe')
        return email

    def save(self, *args, **kwargs):
        email = self.cleaned_data['email']
        self.instance.username = email
        user = super(UserForm, self).save(*args, **kwargs)
        user.set_password(self.cleaned_data['password'])
        user.save()
        return user

    class Meta:
        model = User
        fields = ('email', 'first_name', 'password')


class FollowerForm(ModelForm):
    
    def __init__(self, *args, **kwargs):
        self._follower = kwargs.pop('follower', None)
        super(FollowerForm, self).__init__(*args, **kwargs)
    
    def clean(self):
        if self._follower is None or not self._follower.is_authenticated:
            raise forms.ValidationError(_('Unauthenticated user'))
        elif Follower.objects.filter(follower=self._follower, me=self.cleaned_data['me']).exists():
            raise forms.ValidationError(_('You already follow this user'))
        return self.cleaned_data
    
    def save(self, *args, **kwargs):
        if self._follower is None:
            return None
        self.instance.follower = self._follower
        return super(FollowerForm, self).save(*args, **kwargs)
    
    class Meta:
        model = Follower
        fields = ['me']
