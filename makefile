run_dev:
	docker-compose -f docker-compose.override.yml -f docker-compose.yml up --build --remove-orphans

makemigrations:
	docker-compose -f dev.yml exec efilm-service-dev python manage.py makemigrations
	exit 0
	
migrate:
	docker-compose -f dev.yml exec efilm-service-dev python manage.py migrate
	exit 0

shell:
	docker-compose -f docker-compose.yml exec z1_service bash
	exit 0